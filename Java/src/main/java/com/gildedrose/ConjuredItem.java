package com.gildedrose;

public class ConjuredItem extends Item {
   public ConjuredItem(String name, int sellIn, int quality) {
      super(name, sellIn, quality);
   }

   @Override
   public void ageItem() {
      if (quality > 0) {
         quality = quality - 2;
      }
      sellIn = sellIn - 1;
      if (sellIn < 0) {
         if (quality > 0) {
            quality = quality - 2;
         }
      }
   }
}
