package com.gildedrose;

public abstract class Item {

    public static final String AGED_BRIE = "Aged Brie";
    public static final String BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert";
    public static final String SULFURAS = "Sulfuras, Hand of Ragnaros";
    public String name;

    public int sellIn;

    public int quality;

    protected Item(String name, int sellIn, int quality) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }

    public static Item createItem(String name, int sellIn, int quality) {
        switch (name) {
            case "Conjured Mana Cake":
                return new ConjuredItem(name, sellIn, quality);
            case SULFURAS:
                return new SulfurasItem(name, sellIn, quality);
            case AGED_BRIE:
                return new AgedBrieItem(name, sellIn, quality);
            case BACKSTAGE_PASSES:
                return new BackstageItem(name, sellIn, quality);
            default:
                return new RegularItem(name, sellIn, quality);
        }
    }

    @Override
    public String toString() {
        return this.name + ", " + this.sellIn + ", " + this.quality;
    }

    public abstract void ageItem();
}
