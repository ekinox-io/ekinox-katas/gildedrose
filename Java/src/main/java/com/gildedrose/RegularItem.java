package com.gildedrose;

public class RegularItem extends Item {
    protected RegularItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    public void ageItem() {
        if (quality > 0) {
            quality = quality - 1;
        }
        sellIn = sellIn - 1;
        if (sellIn < 0) {
            if (quality > 0) {
                quality = quality - 1;
            }
        }
    }
}
