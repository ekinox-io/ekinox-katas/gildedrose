package com.gildedrose;

import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class GildedRoseTest {

    @Test
    void should_comply_to_golden_master() throws Exception {
        // When
        final String goldenMasterOutput = TexttestFixture.goldenMasterExecution();

        // Then
        final String goldenMasterFileName = "golden-master.out";
        final List<String> goldenMasterLines = readResourceLines(goldenMasterFileName);
        final String[] splitGoldenMasterOutput = goldenMasterOutput.split(System.lineSeparator());
        assertThat(splitGoldenMasterOutput).containsExactlyElementsOf(goldenMasterLines);
    }


    //TODO "ajouter le test qui ne réduit pas la qualité si on est déjà à 0" - Sylvain Lequeux, le 09/07/2021, lors d'une session
    // de l'incroyable session de refactoring

    private List<String> readResourceLines(String resourceFileName) throws IOException, URISyntaxException {
        final URL resourceFileUrl = GildedRoseTest.class.getClassLoader().getResource(resourceFileName);
        return Files.readAllLines(Paths.get(resourceFileUrl.toURI()));
    }
}
