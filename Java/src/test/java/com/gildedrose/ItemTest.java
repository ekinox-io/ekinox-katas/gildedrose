package com.gildedrose;

import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ItemTest {

   @Test
   void should_decrease_quality_by_1_when_ageing_a_regular_item_given_a_positive_sellin() {
      // Given
      final int sellIn = 10;
      final int quality = 100;
      final Item regularItem = Item.createItem("Regular Item", sellIn, quality);

      // When
      regularItem.ageItem();

      // Then
      assertThat(regularItem.quality).isEqualTo(quality - 1);
   }

   @Test
   void should_decrease_quality_by_2_when_ageing_a_regular_item_given_a_sellin_of_0() {
      // Given
      final int sellIn = 0;
      final int quality = 100;
      final Item regularItem = Item.createItem("Regular Item", sellIn, quality);

      // When
      regularItem.ageItem();

      // Then
      assertThat(regularItem.quality).isEqualTo(quality - 2);
   }

   @Test
   void should_decrease_quality_by_2_when_ageing_a_regular_item_given_a_negative_sellin() {
      // Given
      final int sellIn = -1;
      final int quality = 100;
      final Item regularItem = Item.createItem("Regular Item", sellIn, quality);

      // When
      regularItem.ageItem();

      // Then
      assertThat(regularItem.quality).isEqualTo(quality - 2);
   }

   @Test
   void should_decrease_sellin_by_1_when_ageing_regular_item() {
      // Given
      final int sellIn = -1;
      final int quality = 100;
      final Item regularItem = Item.createItem("Regular Item", sellIn, quality);

      // When
      regularItem.ageItem();

      // Then
      assertThat(regularItem.sellIn).isEqualTo(sellIn - 1);
   }


    @Test
    void should_decrease_quality_by_2_when_ageing_conjured_item_give_a_positive_sellin() {
        // Given
        final int sellIn = 4;
        final int quality = 100;
        final Item conjuredItem = Item.createItem("Conjured Mana Cake", sellIn, quality);

        // When
        conjuredItem.ageItem();

        // Then
        assertThat(conjuredItem.quality).isEqualTo(quality - 2);
    }

   @Test
   void should_decrease_quality_by_4_when_ageing_a_conjured_item_given_a_sellin_of_0() {
      // Given
      final int sellIn = 0;
      final int quality = 100;
      final Item regularItem = Item.createItem("Conjured Mana Cake", sellIn, quality);

      // When
      regularItem.ageItem();

      // Then
      assertThat(regularItem.quality).isEqualTo(quality - 4);
   }

   @Test
   void should_decrease_quality_by_4_when_ageing_a_conjured_item_given_a_negative_sellin() {
      // Given
      final int sellIn = -1;
      final int quality = 100;
      final Item regularItem = Item.createItem("Conjured Mana Cake", sellIn, quality);

      // When
      regularItem.ageItem();

      // Then
      assertThat(regularItem.quality).isEqualTo(quality - 4);
   }

   @Test
   void should_decrease_sellin_by_1_when_ageing_conjured_item() {
      // Given
      final int sellIn = -1;
      final int quality = 100;
      final Item regularItem = Item.createItem("Conjured Mana Cake", sellIn, quality);

      // When
      regularItem.ageItem();

      // Then
      assertThat(regularItem.sellIn).isEqualTo(sellIn - 1);
   }

   // TODO next time : Does TDD implies having only tests by example ?
   // TODO next time : Are tests above by example or testing global rules ?

   @Test
   void should_decrease_quality_twice_as_fast_as_regular_item_when_ageing_a_conjured_item_given_a_negative_sellin() {
      // TODO turn this test into Property Based Testing
      // Given
      final int sellIn = -1;
      final int quality = 100;
      final Item regularItem = Item.createItem("Mana Cake", sellIn, quality);
      final Item conjuredItem = Item.createItem("Conjured Mana Cake", sellIn, quality);

      // When
      regularItem.ageItem();
      conjuredItem.ageItem();

      // Then
      assertThat((quality - regularItem.quality) * 2).isEqualTo(quality - conjuredItem.quality);
   }
}
